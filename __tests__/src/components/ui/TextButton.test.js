import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import TextButton from '../../../../src/components/ui/TextButton';

describe('All tests related to the button component', () => {
  it('Can run the function passed as a prop', () => {
    const onSubmitMock = jest.fn();
    const {getByTestId} = render(
      <TextButton
        testID="formButton"
        onSubmit={onSubmitMock}
        buttonText="Hi btn"
      />,
    );

    fireEvent.press(getByTestId('formButton'));
    expect(onSubmitMock).toHaveBeenCalled();
    expect(onSubmitMock).toHaveBeenCalledTimes(1);
    expect(onSubmitMock).not.toHaveBeenCalledTimes(3);
  });

  it('Can render the button text passed as a prop', () => {
    const onSubmitMock = jest.fn();
    const {getByTestId} = render(
      <TextButton
        testID="formButton"
        onSubmit={onSubmitMock}
        buttonText="say hello"
      />,
    );

    expect(getByTestId('formButton').props.children[0].props.children).toMatch(
      /say hello/,
    );
    expect(
      getByTestId('formButton').props.children[0].props.children,
    ).not.toMatch(/goodbye/);
  });
});
