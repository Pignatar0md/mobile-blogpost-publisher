import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import TextField from '../../../../src/components/ui/TextField';

describe('All the tests related to the TextField component', () => {
  test('The TextInput value changes while the user write on it', () => {
    const onChangeTextMock = jest.fn();

    const {getByTestId} = render(
      <TextField
        testID="formInputText"
        value={'6'}
        onChangeText={onChangeTextMock}
      />,
    );

    fireEvent.changeText(getByTestId('formInputText'), '9');
    expect(onChangeTextMock).toHaveBeenCalledWith('9');
    expect(onChangeTextMock).not.toHaveBeenCalledWith('abc');
  });

  test('The TextInput renders the passed value', () => {
    const onChangeTextMock = jest.fn();

    const {getByTestId} = render(
      <TextField
        testID="formInputText"
        value={'6'}
        onChangeText={onChangeTextMock}
      />,
    );

    expect(getByTestId('formInputText').props.value).toBe('6');
  });
});
