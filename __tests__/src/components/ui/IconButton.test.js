import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import IconButton from '../../../../src/components/ui/IconButton';
import Icon from 'react-native-vector-icons/Feather';

describe('All tests related to the button component', () => {
  it('Can run the function passed as a prop', () => {
    const onSubmitMock = jest.fn();
    const {getByTestId} = render(
      <IconButton testID="formButton" onSubmit={onSubmitMock}>
        <Icon name="plus" style={{fontSize: 18}} />
      </IconButton>,
    );

    fireEvent.press(getByTestId('formButton'));
    expect(onSubmitMock).toHaveBeenCalled();
    expect(onSubmitMock).toHaveBeenCalledTimes(1);
    expect(onSubmitMock).not.toHaveBeenCalledTimes(3);
  });

  it('Can render the icon passed as a prop', () => {
    const onSubmitMock = jest.fn();
    const {getByTestId} = render(
      <IconButton testID="formButton" onSubmit={onSubmitMock}>
        <Icon name="plus" style={{fontSize: 18}} />
      </IconButton>,
    );

    expect(getByTestId('formButton').props.children[0].props.name).toMatch(
      /plus/,
    );
    expect(getByTestId('formButton').props.children[0].props.name).not.toMatch(
      /add/,
    );
  });
});
