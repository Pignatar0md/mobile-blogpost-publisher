import {render} from '@testing-library/react-native';
import BlogPostForm from '../../../src/components/BlogPostForm';

test('Does not shows and error message when all inputs are not filled', () => {
  const onSubmitMock = jest.fn();
  const {getByTestId} = render(
    <BlogPostForm
      initialValues={{title: '', content: ''}}
      onSubmit={onSubmitMock}
      errorMessage={null}
    />,
  );
  expect(getByTestId('blogForm.ErrorMessage').props.children).toBe(null);
  expect(getByTestId('blogForm.ErrorMessage').props.children).not.toBe(
    'Check input fields',
  );
});

test('Shows and error message when all inputs are not filled', () => {
  const onSubmitMock = jest.fn();
  const {getByTestId} = render(
    <BlogPostForm
      initialValues={{title: '', content: ''}}
      onSubmit={onSubmitMock}
      errorMessage={'Check input fields'}
    />,
  );
  expect(getByTestId('blogForm.ErrorMessage').props.children).toBe(
    'Check input fields',
  );
  expect(getByTestId('blogForm.ErrorMessage').props.children).not.toBe(null);
});
