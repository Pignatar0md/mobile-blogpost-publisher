import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import {Provider} from '../../../src/context/BlogContext';
import EditScreen from '../../../src/screens/EditScreen';

describe('All tests related to the EditScreen component', () => {
  test('Shows an error if submit is pressed but textfields are not filled', () => {
    const {getByTestId} = render(<EditScreen route={{params: {postId: 1}}} />, {
      wrapper: Provider,
    });

    expect(getByTestId('blogForm.ErrorMessage').props.children).toBeNull();

    fireEvent.press(getByTestId('submitForm'));

    expect(getByTestId('blogForm.ErrorMessage').props.children).toMatch(
      /Title or content were not provided/,
    );
    expect(getByTestId('blogForm.ErrorMessage').props.children).not.toMatch(
      /lalala/,
    );
  });
});
