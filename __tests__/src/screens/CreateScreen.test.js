import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import CreateScreen from '../../../src/screens/CreateScreen';
import {Provider} from '../../../src/context/BlogContext';

describe('tests of the CreateBlogpost screen', () => {
  test('Shows an error if submit is pressed but textfields are not filled', () => {
    const {getByTestId} = render(<CreateScreen />, {wrapper: Provider});

    expect(getByTestId('blogForm.ErrorMessage').props.children).toBeNull();

    fireEvent.press(getByTestId('submitForm'));

    expect(getByTestId('blogForm.ErrorMessage').props.children).toMatch(
      /Title or content are not provided/,
    );
    expect(getByTestId('blogForm.ErrorMessage').props.children).not.toMatch(
      /lalala/,
    );

    fireEvent.changeText(getByTestId('titleBlogPostField'), 'abc abc');
    fireEvent.press(getByTestId('submitForm'));
    expect(getByTestId('blogForm.ErrorMessage').props.children).toBeNull();
  });
});
