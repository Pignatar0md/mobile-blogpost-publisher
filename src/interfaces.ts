import {BlogPost, initialBlogPostsState} from './types';

export interface BlogPostCtx {
  state: initialBlogPostsState;
  getBlogPosts: () => void;
  addBlogPost: (a: string, b: string, c: () => void) => void;
  deleteBlogPost: (item: BlogPost) => void;
  editBlogPost: (a: string, b: string, c: string, d: () => void) => void;
}
