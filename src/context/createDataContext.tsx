import React, {createContext, useReducer} from 'react';

interface props {
  children: JSX.Element | JSX.Element[];
}
type Action = {type: string; payload?: unknown};
type Reducer = (state: {}, action: Action) => {};
// type Actions = {
//   getBlogPosts: () => void;
//   addBlogPost: () => void;
//   deleteBlogPost: () => void;
//   .
//   .
//   .
//   all the functions passed as a second parameters(inside an object) when createDataContext is called
// };

const createDataContext = (reducer: Reducer, actions: {}, initialState: {}) => {
  const Context = createContext({} as unknown);

  const Provider = ({children}: props) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const boundActions: any = {};
    for (let key in actions) {
      boundActions[key] = actions[key](dispatch);
    }

    return (
      <Context.Provider value={{state, ...boundActions}}>
        {children}
      </Context.Provider>
    );
  };

  return {Context, Provider};
};

export default createDataContext;
