import endpoint from '../api/endpoint';
import {blogpostActionType} from '../enums';
import {initialBlogPostsState} from '../types';
import {blogReducer} from './blogReducer';
import createDataContext from './createDataContext';

export const initialState: initialBlogPostsState = {
  blogPosts: [],
  loading: false,
  errorMessage: '',
};

const getBlogPosts = (dispatch: any) => async () => {
  dispatch({type: blogpostActionType.get_blog_post});
  try {
    const result = await endpoint.get('/blogposts.json');
    const blogPosts = [];
    for (const key in result.data) {
      const blogPost = {id: key, ...result.data[key]};
      blogPosts.push(blogPost);
    }
    dispatch({
      type: blogpostActionType.get_blog_post_success,
      payload: blogPosts,
    });
  } catch (error: any) {
    dispatch({
      type: blogpostActionType.get_blog_post_error,
      payload: error.message,
    });
  }
};

const addBlogPost =
  (dispatch: any) =>
  async (title: string, content: string, callback: () => void) => {
    try {
      dispatch({
        type: blogpostActionType.add_blog_post,
      });
      await endpoint.post('/blogposts.json', {title, content});
      dispatch({
        type: blogpostActionType.add_blog_post_success,
      });
      callback && callback();
    } catch (error: any) {
      dispatch({
        type: blogpostActionType.add_blog_post_error,
        payload: error.message,
      });
    }
  };

const deleteBlogPost =
  () =>
  async ({id}: {id: string}) =>
    await endpoint.delete(`/blogposts/${id}.json`);

const editBlogPost =
  () =>
  async (id: string, title: string, content: string, callback: () => void) => {
    await endpoint.put(`/blogposts/${id}.json`, {
      title,
      content,
    });
    callback && callback();
  };

export const {Context, Provider} = createDataContext(
  blogReducer,
  {getBlogPosts, addBlogPost, deleteBlogPost, editBlogPost},
  initialState,
);
