import {blogpostActionType} from '../enums';
import {Action} from '../types';

export const blogReducer = (state: any, action: Action) => {
  switch (action.type) {
    case blogpostActionType.get_blog_post:
      return {...state, loading: true};
    case blogpostActionType.get_blog_post_success:
      return {
        ...state,
        loading: false,
        errorMessage: '',
        blogPosts: [...action.payload],
      };
    case blogpostActionType.get_blog_post_error:
      return {...state, loading: false, errorMessage: action.payload};
    case blogpostActionType.add_blog_post:
      return {
        ...state,
        loading: true,
      };
    case blogpostActionType.add_blog_post_success:
      return {
        ...state,
        loading: false,
        error: '',
      };
    case blogpostActionType.add_blog_post_error:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};
