import {commentsblogActionType} from '../enums';
import {commentsBlogReducer} from './commentsBlogReducer';
import createDataContext from './createDataContext';

const addCommentBlog =
  (dispatch: any) => (title: string, content: string, callback: () => void) => {
    dispatch({
      type: commentsblogActionType.add_comment_blog,
      payload: {title, content},
    });
    callback && callback();
  };

export const {Context, Provider} = createDataContext(
  commentsBlogReducer,
  {addCommentBlog},
  {},
);
