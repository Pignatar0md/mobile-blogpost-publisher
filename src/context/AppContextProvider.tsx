import React, {ComponentProps, FC} from 'react';
import {Provider as BlogProvider} from './BlogContext';
import {Provider as CommentsBlogProvider} from './CommentsBlogContext';

const providers = [BlogProvider, CommentsBlogProvider];

const combineContexts = (...contexts: FC[]): FC => {
  return contexts.reduce((AccumulatedContexts, CurrentContext) => {
    return ({children}: {children: ComponentProps<FC>}): JSX.Element => (
      <AccumulatedContexts>
        <CurrentContext>{children}</CurrentContext>
      </AccumulatedContexts>
    );
  });
};

export const AppContextProvider = combineContexts(...(providers as []));
