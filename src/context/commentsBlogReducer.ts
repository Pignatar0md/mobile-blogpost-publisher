import {commentsblogActionType} from '../enums';
import {Action} from '../types';

export const commentsBlogReducer = (state: any, action: Action) => {
  switch (action.type) {
    case commentsblogActionType.add_comment_blog:
      return {...state, commentsBlog: [...state.commentsBlog, action.payload]};
    default:
      return state;
  }
};
