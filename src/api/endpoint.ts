import axios from 'axios';

export default axios.create({
  baseURL:
    'https://blogposts-rn-default-rtdb.europe-west1.firebasedatabase.app',
});
