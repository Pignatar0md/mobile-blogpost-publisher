import {useContext} from 'react';
import {Context} from '../context/BlogContext';
import {BlogPostCtx} from '../interfaces';

export const useBlogPosts = () => {
  const blogPostContext: BlogPostCtx = useContext(Context);
  const {getBlogPosts, addBlogPost, deleteBlogPost, editBlogPost} =
    blogPostContext;
  const getBlogPostById = (postId: string) =>
    blogPostContext.state.blogPosts.find(({id}: {id: string}) => id === postId);

  return {
    blogPosts: blogPostContext.state.blogPosts,
    addBlogPost,
    getBlogPosts,
    getBlogPostById,
    deleteBlogPost,
    editBlogPost,
  };
};
