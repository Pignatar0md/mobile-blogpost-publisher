import {useContext} from 'react';
import {Context} from '../context/CommentsBlogContext';

export const useCommensBlog = () => {
  const {state, addCommentsBlog} = useContext(Context);

  return {
    state,
    addCommentsBlog,
  };
};
