export interface initialBlogPostsState {
  blogPosts: BlogPost[];
  loading: boolean;
  errorMessage: string;
}

export type BlogPost = {
  id: string;
  title: string;
  content: string;
};

export type Action = {
  type: string;
  payload?: unknown;
};
