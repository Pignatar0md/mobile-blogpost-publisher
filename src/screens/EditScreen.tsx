import React, {useState} from 'react';
import BlogPostForm from '../components/BlogPostForm';
import {useBlogPosts} from '../hooks/useBlogPosts';
import {checkValidInputs} from '../utils/utils';
import {Navigation, Route} from './types';

const EditScreen = ({
  navigation,
  route,
}: {
  navigation: Navigation;
  route: Route;
}) => {
  const {getBlogPostById, editBlogPost} = useBlogPosts();
  const [errorMessage, setErrorMessage] = useState<null | string>(null);
  const {
    params: {postId},
  } = route;
  const blogPost = getBlogPostById(postId);

  const updateBlogPost = (title: string, content: string) => {
    checkValidInputs({
      title,
      content,
      callback: setErrorMessage,
      cbParams: null,
    })
      ? editBlogPost(postId, title, content, () => navigation.goBack())
      : setErrorMessage('Title or content were not provided');
  };

  return (
    <BlogPostForm
      initialValues={blogPost}
      onSubmit={updateBlogPost}
      errorMessage={errorMessage}
    />
  );
};

export default EditScreen;
