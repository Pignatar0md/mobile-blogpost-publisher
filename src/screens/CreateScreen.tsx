import React, {useState} from 'react';
import BlogPostForm from '../components/BlogPostForm';
import {useBlogPosts} from '../hooks/useBlogPosts';
import {checkValidInputs} from '../utils/utils';
import {Navigation} from './types';

const CreateScreen = ({navigation}: {navigation: Navigation}) => {
  const {addBlogPost} = useBlogPosts();
  const [errorMessage, setErrorMessage] = useState<null | string>(null);

  const saveBlogPost = (title: string, content: string) => {
    checkValidInputs({
      title,
      content,
      callback: setErrorMessage,
      cbParams: null,
    })
      ? addBlogPost(title, content, () => navigation.navigate('Home'))
      : setErrorMessage('Title or content are not provided');
  };

  return <BlogPostForm onSubmit={saveBlogPost} errorMessage={errorMessage} />;
};

export default CreateScreen;
