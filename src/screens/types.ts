import {ReactElement} from 'react';

export type Navigation = {
  navigate: (a: string, b?: {}) => void;
  goBack: () => void;
  setOptions: (a: {}) => ReactElement;
};

export type Route = {
  params: {
    postId: string;
  };
};
