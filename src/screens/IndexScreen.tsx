import React, {useEffect, useLayoutEffect} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {BlogPost} from '../types';
import {Navigation} from './types';
import Icon from 'react-native-vector-icons/Feather';
import {useBlogPosts} from '../hooks/useBlogPosts';
import IconButton from '../components/ui/IconButton';

const IndexScreen = ({navigation}: {navigation: Navigation}) => {
  const {blogPosts, getBlogPosts, deleteBlogPost} = useBlogPosts();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <IconButton onSubmit={() => navigation.navigate('New BlogPost')}>
          <Icon name="plus" style={{fontSize: 18}} />
        </IconButton>
      ),
    });
  }, [navigation]);

  useEffect(() => {
    async function getBlogPostsList() {
      await getBlogPosts();
    }
    getBlogPostsList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [blogPosts]);

  const renderBlogPost = ({item}: {item: BlogPost}) => {
    return (
      <IconButton
        key={item?.id}
        onSubmit={() => navigation.navigate('BlogPost', {postId: item.id})}>
        <View style={styles.row}>
          <Text style={styles.title}>
            #{item?.id?.substring(0, 3)} {item.title}
          </Text>
          <IconButton onSubmit={() => deleteBlogPost(item)}>
            <Icon name="trash" style={styles.icon} />
          </IconButton>
        </View>
      </IconButton>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={blogPosts}
        keyExtractor={item => item?.id?.toString()}
        renderItem={renderBlogPost}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {fontSize: 18},
  icon: {
    fontSize: 24,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 20,
    borderTopWidth: 1,
    borderColor: 'gray',
    paddingHorizontal: 20,
  },
});

export default IndexScreen;
