import React, {useLayoutEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import IconButton from '../components/ui/IconButton';
import {useBlogPosts} from '../hooks/useBlogPosts';
import {Navigation, Route} from './types';

const ShowScreen = ({
  navigation,
  route,
}: {
  navigation: Navigation;
  route: Route;
}) => {
  const {params} = route;
  const {getBlogPostById} = useBlogPosts();
  const blogPost = getBlogPostById(params.postId);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <IconButton
          onSubmit={() =>
            navigation.navigate('Edit BlogPost', {postId: params.postId})
          }>
          <Icon name="edit" style={{fontSize: 18}} />
        </IconButton>
      ),
    });
  }, [navigation, params.postId]);

  return (
    <View>
      <Text>{blogPost!.title}</Text>
      <Text>{blogPost!.content}</Text>
    </View>
  );
};

const styles = StyleSheet.create({});

export default ShowScreen;
