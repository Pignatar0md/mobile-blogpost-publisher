import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {BlogPostFrm} from './interfaces';
import Tag from './ui/Tag';
import TextButton from './ui/TextButton';
import TextField from './ui/TextField';

const BlogPostForm = ({onSubmit, initialValues, errorMessage}: BlogPostFrm) => {
  const [title, setTitle] = useState(initialValues?.title ?? '');
  const [content, setContent] = useState(initialValues?.content ?? '');

  return (
    <View>
      <Tag text="Enter title:" />
      <TextField
        testID="titleBlogPostField"
        value={title}
        onChangeText={(text: string) => setTitle(text)}
      />
      <Tag text="Enter content:" />
      <TextField
        testID="contentBlogPostField"
        value={content}
        onChangeText={(text: string) => setContent(text)}
      />
      <TextButton
        testID="submitForm"
        onSubmit={() => onSubmit(title, content)}
        buttonText="Save BlogPost"
      />
      <Text testID="blogForm.ErrorMessage" style={styles.errorMessage}>
        {errorMessage}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  errorMessage: {
    color: 'red',
  },
});

export default BlogPostForm;
