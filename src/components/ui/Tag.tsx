import React, {FC} from 'react';
import {StyleSheet, Text} from 'react-native';

const Tag: FC<{text: string}> = ({text}) => {
  return <Text style={styles.label}>{text}</Text>;
};

const styles = StyleSheet.create({
  label: {
    fontSize: 20,
    marginBottom: 5,
    marginLeft: 5,
  },
});

export default Tag;
