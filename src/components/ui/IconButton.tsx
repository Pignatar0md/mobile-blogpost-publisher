import React, {FC, ReactElement} from 'react';
import {GestureResponderEvent, TouchableOpacity} from 'react-native';

const IconButton: FC<{
  children: ReactElement;
  onSubmit: (event: GestureResponderEvent) => void;
  testID?: string;
}> = ({children, onSubmit, ...props}) => {
  return (
    <TouchableOpacity onPress={onSubmit} {...props}>
      {children}
    </TouchableOpacity>
  );
};

export default IconButton;
