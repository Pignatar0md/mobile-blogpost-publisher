import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';

type FormInput = {
  onChangeText: (a: string) => void;
  value: string;
  testID: string;
};

const TextField = ({onChangeText, value, ...props}: FormInput) => {
  return (
    <View>
      <TextInput
        value={value}
        style={styles.input}
        onChangeText={onChangeText}
        {...props}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    fontSize: 18,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 15,
    padding: 5,
    margin: 5,
  },
});

export default TextField;
