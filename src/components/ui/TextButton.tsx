import React, {FC} from 'react';
import {GestureResponderEvent, Text, TouchableOpacity} from 'react-native';

const TextButton: FC<{
  buttonText: string;
  onSubmit: (event: GestureResponderEvent) => void;
  testID?: string;
}> = ({buttonText, onSubmit, ...props}) => {
  return (
    <TouchableOpacity onPress={onSubmit} {...props}>
      <Text>{buttonText}</Text>
    </TouchableOpacity>
  );
};

export default TextButton;
