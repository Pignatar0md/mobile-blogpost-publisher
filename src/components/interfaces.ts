export interface BlogPostFrm {
  onSubmit: (title: string, content: string) => void;
  initialValues?: {title: string; content: string};
  errorMessage: null | string;
}
