import React from 'react';

type ValidInputs = {
  title: string;
  content: string;
  callback?: (a: React.SetStateAction<string | null>) => void;
  cbParams: string | null;
};

export const checkValidInputs = ({
  title,
  content,
  callback,
  cbParams,
}: ValidInputs): boolean => {
  if (title || content) {
    callback && callback(cbParams);
    return true;
  }
  return false;
};
