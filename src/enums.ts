export enum blogpostActionType {
  add_blog_post = 'ADD_BLOGPOST',
  add_blog_post_success = 'ADD_BLOGPOST_SUCCESS',
  add_blog_post_error = 'ADD_BLOGPOST_ERROR',
  get_blog_post = 'GET_BLOGPOST',
  get_blog_post_success = 'GET_BLOGPOST_SUCCESS',
  get_blog_post_error = 'GET_BLOGPOST_ERROR',
}

export enum commentsblogActionType {
  add_comment_blog = 'ADD_COMMENTBLOG',
  add_comment_blog_success = 'ADD_COMMENTBLOG_SUCCESS',
  add_comment_blog_error = 'ADD_COMMENTBLOG_ERROR',
  get_blog_post = 'GET_BLOGPOST',
  get_blog_post_success = 'GET_BLOGPOST_SUCCESS',
  get_blog_post_error = 'GET_BLOGPOST_ERROR',
}
